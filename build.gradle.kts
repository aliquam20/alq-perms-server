import com.bmuschko.gradle.docker.tasks.image.Dockerfile
import com.bmuschko.gradle.docker.tasks.image.DockerBuildImage
import com.bmuschko.gradle.docker.tasks.image.DockerPushImage
import org.aliquam.AlqUtils

plugins {
    id("org.aliquam.alq-gradle-parent") version "0.4.14"
    id("com.bmuschko.docker-remote-api") version "6.7.0"
}

repositories {
    mavenCentral()
    maven("https://papermc.io/repo/repository/maven-public/")
}

val alq = AlqUtils(project).withStandardProjectSetup()

val artifactId = "alq-perms-server"
val baseVersion = "1.0.0"
group = "org.aliquam"
version = alq.getSemVersion(baseVersion)

val buildWorkDir = "build/work"
val buildDockerDir = "build/docker/work"
val srcServerDir = "src/mcserver"

val branchName: String? = System.getenv("BRANCH_NAME")
val isJenkins = branchName != null
val dockerRegistryHost = if (isJenkins) alq.getEnvOrPropertyOrThrow("DOCKER_REGISTRY_HOST") else null
val dockerImageName = "aliquam/${artifactId}"

val unzip = configurations.create("alqUnzipAtRoot")
val pluginJar = configurations.create("alqCopyJarToPluginsDirectory")
val lib = configurations.create("alqCopyJarToLibDirectory")
dependencies {
    unzip("org.aliquam:alq-server-jumpstarter:0.6.0-DEV_BUILD")
    unzip("io.papermc:paper-server:267")
    unzip("net.milkbowl:vault:1.7.3")
    unzip("net.luckperms:luck-perms-bukkit:5.3.74")
    pluginJar("org.aliquam:alq-perms-plugin:0.0.1-DEV_BUILD")

    lib("org.aliquam:alq-perms-plugin:0.0.1-DEV_BUILD") {
        exclude(group = "com.destroystokyo.paper", module = "paper-api")
        exclude(group = "net.luckperms", module = "api")
    }
}

val buildServer = tasks.register<Copy>("buildServer") {
    from(srcServerDir)
    unzip.asFileTree.forEach {
        from(zipTree(it))
    }
    into(buildWorkDir)

    doLast {
        File("$buildWorkDir/plugins").mkdirs()
        val pjFiles = pluginJar.asFileTree.filter { pjFile ->
            pluginJar.dependencies.any { dep -> pjFile.toString().endsWith("${dep.name}-${dep.version}.jar") }
        }
        copy {
            pjFiles.forEach {
                from(it)
            }
            into("$buildWorkDir/plugins")
        }

        File("$buildWorkDir/lib").mkdirs()
        val libRoots = lib.asFileTree.filter { libFile ->
            lib.dependencies.any { dep -> libFile.toString().endsWith("${dep.name}-${dep.version}.jar") }
        }
        // libRoots.forEach { println("libRoots: ${it}")}
        val libDependencies = lib.resolvedConfiguration.files.filter { !libRoots.contains(it) }
        // libDependencies.forEach { println("libdep: ${it}")}
        copy {
            libDependencies.forEach {
                from(it)
            }
            into("$buildWorkDir/lib")
        }
    }
}

docker {
    if (isJenkins) {
        url.set("unix:///var/run/docker.sock")
        registryCredentials {
            username.set(alq.getEnvOrPropertyOrThrow("DOCKER_REGISTRY_USER"))
            password.set(alq.getEnvOrPropertyOrThrow("DOCKER_REGISTRY_PASS"))
            url.set("https://${dockerRegistryHost}/v2/")
        }
    }
}

val createDockerfile = tasks.register<Dockerfile>("createDockerfile") {
    from("openjdk:15-jdk-slim-buster")
    copyFile("work/", "/server/")
    workingDir("/server/")
    runCommand("apt update && apt -y install python3")
    entryPoint("python3", "@run_server.py")
    exposePort(24001)
}

val buildDockerImage = tasks.register<DockerBuildImage>("buildDockerImage") {
    dependsOn(createDockerfile)
    dependsOn(buildServer)

    doFirst {
        copy {
            from(buildWorkDir)
            into(buildDockerDir)
        }
    }

    when (branchName) {
        "master" -> {
            images.add("${dockerRegistryHost}/${dockerImageName}:latest")
            images.add("${dockerRegistryHost}/${dockerImageName}:${version}")
        }
        "develop" -> {
            images.add("${dockerRegistryHost}/${dockerImageName}:snapshot")
            images.add("${dockerRegistryHost}/${dockerImageName}:${version}")
        }
        else -> {
            images.add("${dockerImageName}:dev_build")
        }
    }
}

val pushDockerImage = tasks.register<DockerPushImage>("pushDockerImage") {
    when (branchName) {
        "master" -> {
            images.add("${dockerRegistryHost}/${dockerImageName}:latest")
            images.add("${dockerRegistryHost}/${dockerImageName}:${version}")
        }
        "develop" -> {
            images.add("${dockerRegistryHost}/${dockerImageName}:snapshot")
            images.add("${dockerRegistryHost}/${dockerImageName}:${version}")
        }
    }
}
