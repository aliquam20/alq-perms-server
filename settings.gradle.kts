rootProject.name = "alq-perms-server"

pluginManagement {
    repositories {
        mavenLocal()
        maven("https://nexus.jeikobu.net/repository/maven-releases/")
        gradlePluginPortal()
    }
}